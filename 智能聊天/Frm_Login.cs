﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 智能聊天
{
    public partial class Frm_Login : Form
    {
        public Frm_Login()
        {
            InitializeComponent();
            this.webBrowser1.DocumentCompleted += WebBrowser1_DocumentCompleted;
        }

        private void WebBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (this.webBrowser1.Url.ToString().Contains("passport.bilibili.com/ajax/miniLogin/redirect"))
            {
                //string cookie = GetCookieString("https://passport.bilibili.com");
                Uri u = new Uri("https://passport.bilibili.com");
                string cookie = FullWebBrowserCookie.GetCookieInternal(u, false);

                plugin.option.Cookie = cookie;

                IniFiles ini = new IniFiles(plugin.iniPath);
                ini.WriteString("Base", "Cookie", cookie);
                this.DialogResult = DialogResult.OK;
            }
        }

        private void Frm_Login_Load(object sender, EventArgs e)
        {
            this.webBrowser1.Navigate("https://passport.bilibili.com/ajax/miniLogin/minilogin?" + DateTime.Now.ToFileTime().ToString());
        }

        private void toolStripButton_RegistryKey_Click(object sender, EventArgs e)
        {
            try
            {
                Process processes = Process.GetCurrentProcess();
                string name = processes.ProcessName + ".exe";
                RegistryKey key = Registry.LocalMachine;
                RegistryKey ie1 = key.OpenSubKey("SOFTWARE\\Microsoft\\Internet Explorer\\MAIN\\FeatureControl\\FEATURE_BROWSER_EMULATION", true);
                ie1.SetValue(name, 10000, RegistryValueKind.DWord);
                RegistryKey ie2 = key.OpenSubKey("SOFTWARE\\Wow6432Node\\Microsoft\\Internet Explorer\\MAIN\\FeatureControl\\FEATURE_BROWSER_EMULATION", true);
                ie2.SetValue(name, 10000, RegistryValueKind.DWord);
                key.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                MessageBox.Show("设置完毕，请重启UP主助手！");
            }
            
        }

        private void toolStripButton_SetCookies_Click(object sender, EventArgs e)
        {
            if (new Frm_Cookies().ShowDialog() == DialogResult.OK)
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        //#region 获取完整Cookie
        //[DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
        //static extern bool InternetGetCookieEx(string pchURL, string pchCookieName, StringBuilder pchCookieData, ref int pcchCookieData, int dwFlags, object lpReserved);
        ///// <summary>
        ///// 取Cookie
        ///// </summary>
        ///// <param name="url"></param>
        ///// <returns></returns>
        //private static string GetCookieString(string url)
        //{
        //    // Determine the size of the cookie      
        //    int datasize = 256;
        //    StringBuilder cookieData = new StringBuilder(datasize);
        //    if (!InternetGetCookieEx(url, null, cookieData, ref datasize, 0x00002000, null))
        //    {
        //        if (datasize < 0)
        //            return null;
        //        // Allocate stringbuilder large enough to hold the cookie    
        //        cookieData = new StringBuilder(datasize);
        //        if (!InternetGetCookieEx(url, null, cookieData, ref datasize, 0x00002000, null))
        //            return null;
        //    }
        //    return cookieData.ToString();
        //}
        //#endregion
    }
}
