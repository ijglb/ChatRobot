﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace 智能聊天
{
    public class BiliBili
    {
        static DateTime lockTime;
        public static plugin pluginInstance;
        private static int[] tipsCount = { 0,0,0,0};

        static BiliBili()
        {
            lockTime = DateTime.Now;
        }

        public static void Work()
        {
            if (lockTime > DateTime.Now)
                return;

            if (string.IsNullOrEmpty(plugin.option.ACCESS_TOKEN) || !CheckCookie())
            {
                if (!LoginPassword())
                    return;
            }
            //else
            //{
            //    LoginToken();//有点混乱直接采用简单粗暴的重新登录方式
            //}

            //CheckCookie();

            lockTime = DateTime.Now.AddSeconds(600);
        }

        static HttpItem GetHttp()
        {
            var item = new HttpItem
            {
                Accept = "*/*",
                ContentType = "application/x-www-form-urlencoded",
                UserAgent = "bili-universal/6680 CFNetwork/897.15 Darwin/17.5.0",
                Referer = string.Format("https://live.bilibili.com/{0}", Operate.RoomID),
            };
            item.Cookie = plugin.option.Cookie;
            //item.Header.Add("Accept-Encoding", "gzip");
            //item.Header.Add("Accept-Language", "zh-cn");
            return item;
        }

        static bool CheckCookie()
        {
            var item = GetHttp();
            item.Method = "GET";
            item.URL = "https://api.live.bilibili.com/User/getUserInfo?ts=" + Time();
            var result = new HttpHelper().GetHtml(item);
            var json = JsonConvert.DeserializeObject<JObject>(result.Html);
            if (json != null)
            {
                if (json["code"] != null && json["code"].ToString() != "0")
                {
                    if (tipsCount[2]<4)
                    {
                        pluginInstance.SendMSG("检测到 Cookie 过期，尝试进行重新登录");
                        tipsCount[2]++;
                    }
                    //GetCookie();
                    //重新登录
                    return false;
                }
                else
                    return true;
            }
            else
            {
                if (tipsCount[3] < 4)
                {
                    pluginInstance.SendMSG("CheckCookie异常，尝试进行重新登录");
                    tipsCount[3]++;
                }
                return false;
            }
        }

        static void GetCookie()
        {
            var item = GetHttp();
            item.Method = "GET";
            item.URL = "https://passport.bilibili.com/api/login/sso?" + BuildHttpQuery(Sign(null));
            var result = new HttpHelper().GetHtml(item);
            plugin.option.Cookie = result.Cookie;//??
            //var json = JsonConvert.DeserializeObject<JObject>(result.Html);
            //if (json != null)
            //{
            //    if (json["code"] != null && json["code"].ToString() != "0")
            //    {
            //        pluginInstance.SendMSG("检测到 Cookie 过期");
            //    }
            //}
            //else
            //{
            //    pluginInstance.SendMSG("CheckCookie异常");
            //}
        }

        static void LoginToken()
        {
            if (!CheckToken())
            {
                pluginInstance.SendMSG("检测到令牌即将过期");
                pluginInstance.SendMSG("申请更换令牌");
                if (!Refresh())
                {
                    pluginInstance.SendMSG("更换令牌失败");
                    pluginInstance.SendMSG("使用帐号密码方式登录");
                    LoginPassword();
                }
            }
        }

        static bool Refresh()
        {
            Dictionary<string, string> payload = new Dictionary<string, string>();
            payload.Add("access_token", plugin.option.ACCESS_TOKEN);
            payload.Add("refresh_token", plugin.option.REFRESH_TOKEN);

            var item = GetHttp();
            item.Method = "POST";
            item.URL = "https://passport.bilibili.com/api/oauth2/refreshToken";
            item.Postdata = BuildHttpQuery(Sign(payload));
            var result = new HttpHelper().GetHtml(item);
            var json = JsonConvert.DeserializeObject<JObject>(result.Html);
            if (json != null)
            {
                if (json["code"] != null && json["code"].ToString() != "0")
                {
                    pluginInstance.SendMSG("续签令牌失败：" + json["message"].ToString());
                    return false;
                }
                else
                {
                    pluginInstance.SendMSG("续签令牌成功");
                    //plugin.option.ACCESS_TOKEN = json["data"]["token_info"]["access_token"].ToString();
                    //plugin.option.REFRESH_TOKEN = json["data"]["token_info"]["refresh_token"].ToString();
                    ParseCookies(json["data"]["cookie_info"]["cookies"].ToString());
                    return true;
                }
            }
            else
            {
                pluginInstance.SendMSG("Refresh异常");
                return false;
            }
        }

        static bool CheckToken()
        {
            Dictionary<string, string> payload = new Dictionary<string, string>();
            payload.Add("access_token", plugin.option.ACCESS_TOKEN);
            var item = GetHttp();
            item.Method = "GET";
            item.URL = "https://passport.bilibili.com/api/oauth2/info?" + BuildHttpQuery(Sign(payload));
            var result = new HttpHelper().GetHtml(item);
            var json = JsonConvert.DeserializeObject<JObject>(result.Html);
            if (json != null)
            {
                if (json["code"] != null && json["code"].ToString() != "0")
                {
                    pluginInstance.SendMSG("令牌验证失败");
                    return false;
                }
                else
                {
                    int ts = int.Parse(json["ts"].ToString());
                    int expires_in = int.Parse(json["data"]["expires_in"].ToString());
                    DateTime exDate = GetTime(ts + expires_in);
                    pluginInstance.SendMSG("令牌验证成功，有效期:" + exDate.ToString("yyyy-MM-dd HH:mm:ss"));
                    return expires_in > 86400;
                }
            }
            else
            {
                pluginInstance.SendMSG("CheckToken异常");
                return false;
            }
        }

        static bool LoginPassword()
        {
            if (string.IsNullOrEmpty(plugin.option.User) || string.IsNullOrEmpty(plugin.option.Pwd))
            {
                if (tipsCount[0]<4)
                {
                    pluginInstance.SendMSG("未在插件中填写帐号密码!");
                    tipsCount[0]++;
                }
                return false;
            }
            var keyStr = GetPublicKey();
            if(!string.IsNullOrEmpty(keyStr))
            {
                var json = JsonConvert.DeserializeObject<JObject>(keyStr);
                string hash = json["data"]["hash"].ToString();
                string publicKey = json["data"]["key"].ToString();
                publicKey = publicKey.Replace("-----BEGIN PUBLIC KEY-----", "").Replace("-----END PUBLIC KEY-----", "").Replace("\n", "").Trim();
                RSACryptoService rsa = new RSACryptoService("", publicKey);
                string crypt = rsa.Encrypt(hash + plugin.option.Pwd);
                return GetToken(plugin.option.User, crypt);
            }
            return false;
        }

        static bool GetToken(string username, string password)
        {
            Dictionary<string, string> payload = new Dictionary<string, string>();
            payload.Add("subid", "1");
            payload.Add("permission", "ALL");
            payload.Add("username", username);
            payload.Add("password", password);
            payload.Add("captcha", "");

            var item = GetHttp();
            item.Method = "POST";
            item.URL = "https://passport.bilibili.com/api/v2/oauth2/login";
            item.Postdata = BuildHttpQuery(Sign(payload));
            var result = new HttpHelper().GetHtml(item);
            var json = JsonConvert.DeserializeObject<JObject>(result.Html);
            if (json != null)
            {
                if (json["code"] != null && json["code"].ToString() != "0")
                {
                    if (tipsCount[1]<4)
                    {
                        pluginInstance.SendMSG("帐号登录失败：" + json["message"].ToString());
                        tipsCount[1]++;

                    }
                    return false;
                }
                else
                {
                    pluginInstance.SendMSG("帐号登录成功");
                    plugin.option.ACCESS_TOKEN = json["data"]["token_info"]["access_token"].ToString();
                    plugin.option.REFRESH_TOKEN = json["data"]["token_info"]["refresh_token"].ToString();
                    ParseCookies(json["data"]["cookie_info"]["cookies"].ToString());
                    return true;
                }
            }
            else
            {
                pluginInstance.SendMSG("GetToken异常");
                return false;
            }
        }

        static void ParseCookies(string jsonCookieArr)
        {
            var jsonArr = JsonConvert.DeserializeObject<JArray>(jsonCookieArr);
            StringBuilder cookieBuilder = new StringBuilder();
            foreach (var item in jsonArr)
            {
                cookieBuilder.AppendFormat("{0}={1}; ", item["name"].ToString(), item["value"].ToString());
            }
            plugin.option.Cookie = cookieBuilder.ToString().TrimEnd().TrimEnd(';');
        }

        static string GetPublicKey()
        {
            var item = GetHttp();
            item.Method = "POST";
            item.URL = "https://passport.bilibili.com/api/oauth2/getKey";
            item.Postdata = BuildHttpQuery(Sign(null));
            var result = new HttpHelper().GetHtml(item);
            var json = JsonConvert.DeserializeObject<JObject>(result.Html);
            if (json != null)
            {
                if (json["code"] != null && json["code"].ToString() != "0")
                {
                    pluginInstance.SendMSG("公钥获取失败：" + json["message"].ToString());
                    return null;
                }
                else
                    return result.Html;
            }
            else
            {
                pluginInstance.SendMSG("GetPublicKey异常");
                return null;
            }
        }

        static Dictionary<string, string> Sign(Dictionary<string, string> payload)
        {
            if (payload == null)
                payload = new Dictionary<string, string>();
            string appkey = "27eb53fc9058f8c3";
            string appsecret = "c2ed53a74eeefe3cf99fbd01d8c9c375";

            Dictionary<string, string> def = new Dictionary<string, string>();
            def.Add("access_key", plugin.option.ACCESS_TOKEN);
            def.Add("actionKey", "appkey");
            def.Add("appkey", appkey);
            def.Add("build", "6680");
            def.Add("device", "phone");
            def.Add("mobi_app", "iphone");
            def.Add("platform", "ios");
            def.Add("ts", Time());

            foreach (var item in payload)
            {
                def.Add(item.Key, item.Value);
            }
            if (def.ContainsKey("sign"))
                def.Remove("sign");
            def = def.OrderBy(m => m.Key).ToDictionary(m => m.Key, m => m.Value);
            var data = BuildHttpQuery(def);
            def.Add("sign", GetMD5(data + appsecret));
            return def;
        }

        static string BuildHttpQuery(Dictionary<string, string> paramDic)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in paramDic)
            {
                sb.AppendFormat("{0}={1}&", item.Key, Uri.EscapeDataString(item.Value));
            }
            return sb.ToString().TrimEnd('&');
        }

        static string Time()
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return ((int)(DateTime.Now - startTime).TotalSeconds).ToString();
        }

        static DateTime GetTime(int timeStamp)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            return dtStart.AddSeconds(timeStamp);
        }

        static string GetMD5(string source)
        {
            byte[] sor = Encoding.UTF8.GetBytes(source);
            MD5 md5 = MD5.Create();
            byte[] result = md5.ComputeHash(sor);
            StringBuilder strbul = new StringBuilder(40);
            for (int i = 0; i < result.Length; i++)
            {
                strbul.Append(result[i].ToString("x2"));//加密结果"x2"结果为32位,"x3"结果为48位,"x4"结果为64位

            }
            return strbul.ToString();
        }
    }
}
