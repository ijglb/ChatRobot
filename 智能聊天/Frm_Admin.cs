﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 智能聊天
{
    public partial class Frm_Admin : Form
    {
        private int listview = 0;

        public Frm_Admin()
        {
            InitializeComponent();
            this.listView_GList.MouseDown += ListView_GList_MouseDown;
            this.listView_FK.MouseDown += ListView_FK_MouseDown;
            this.listView_QA.MouseDown += ListView_QA_MouseDown;
        }

        private void Frm_Admin_Load(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(plugin.option.Cookie))
            //    this.button_Bili.Text = "B站帐号登陆";
            //else
            //    this.button_Bili.Text = "重登B站帐号";
            this.textBox_User.Text = plugin.option.User;
            this.textBox_Password.Text = plugin.option.Pwd;
            this.textBox_Cookies.Text = plugin.option.Cookie;
            this.checkBox_Cookies.Checked = plugin.option.UseCookie;

            if (plugin.option.DMZS == 30)
                this.radioButton_30.Checked = true;
            else
                this.radioButton_20.Checked = true;

            this.checkBox_LY.Checked = plugin.option.IsHYLY;
            this.textBox_LY.Text = plugin.option.LYMB;

            this.checkBox_TL.Checked = plugin.option.IsTL;
            this.textBox_ApiKey.Text = plugin.option.TLApi;
            this.textBox_KeyWords.Text = plugin.option.TLKeyWords.Replace("|", "/");

            this.checkBox_TkGift.Checked = plugin.option.IsTKGift;
            this.checkBox_DMould.Checked = plugin.option.IsGDMB;
            this.textBox_DMould.Text = plugin.option.GDefaultMB;
            this.textBox_GInterval.Text = (plugin.option.GInterval / 1000).ToString();
            listView_GList.Items.Clear();
            foreach (var mb in plugin.option.GMBList)
            {
                ListViewItem item = listView_GList.Items.Add(mb.Name);
                item.SubItems.Add(mb.MB);
            }

            this.checkBox_AutoFK.Checked = plugin.option.IsAutoFK;
            this.textBox_FKInterval.Text = (plugin.option.FKInterval / 1000).ToString();
            listView_FK.Items.Clear();
            foreach (var str in plugin.option.FKList)
            {
                listView_FK.Items.Add(str);
            }

            checkBox_QA.Checked = plugin.option.IsQA;
            textBox_Like.Text = plugin.option.QLike.ToString();
            listView_QA.Items.Clear();
            foreach (var qa in plugin.option.QAList)
            {
                ListViewItem item = listView_QA.Items.Add(qa.Q);
                item.SubItems.Add(qa.A);
            }
        }

        private void Frm_Admin_FormClosing(object sender, FormClosingEventArgs e)
        {
            plugin.option.User = textBox_User.Text.Trim();
            plugin.option.Pwd = textBox_Password.Text.Trim();
            plugin.option.Cookie = textBox_Cookies.Text.Trim();
            plugin.option.UseCookie = checkBox_Cookies.Checked;
            //弹幕字数
            plugin.option.DMZS = radioButton_20.Checked ? 20 : 30;

            //弹幕欢迎老爷
            plugin.option.IsHYLY = checkBox_LY.Checked;
            plugin.option.LYMB = textBox_LY.Text.Trim();

            //图灵
            plugin.option.IsTL = checkBox_TL.Checked;
            plugin.option.TLApi = textBox_ApiKey.Text.Trim();
            plugin.option.TLKeyWords = textBox_KeyWords.Text.Trim().Replace("/", "|");

            //弹幕感谢
            plugin.option.IsTKGift = checkBox_TkGift.Checked;
            plugin.option.IsGDMB = checkBox_DMould.Checked;
            plugin.option.GDefaultMB = textBox_DMould.Text.Trim();
            try
            {
                int i = int.Parse(textBox_GInterval.Text.Trim()) * 1000;
                plugin.option.GInterval = i;
            }
            catch (Exception)
            {
                plugin.option.GInterval = 5000;
            }
            plugin.option.GMBList.Clear();
            for (int i = 0; i < listView_GList.Items.Count; i++)
            {
                string name = listView_GList.Items[i].SubItems[0].Text;
                string str = listView_GList.Items[i].SubItems[1].Text;
                GiftMB mb = new GiftMB();
                mb.Name = name;
                mb.MB = str;
                plugin.option.GMBList.Add(mb);
            }

            //自动发言
            plugin.option.IsAutoFK = checkBox_AutoFK.Checked;
            try
            {
                int i = int.Parse(textBox_FKInterval.Text.Trim()) * 1000;
                plugin.option.FKInterval = i;
            }
            catch (Exception)
            {
                plugin.option.FKInterval = 5000;
            }
            plugin.option.FKList.Clear();
            for (int i = 0; i < listView_FK.Items.Count; i++)
            {
                string fk = listView_FK.Items[i].Text;
                plugin.option.FKList.Add(fk);
            }

            //智能回答
            plugin.option.IsQA = checkBox_QA.Checked;
            try
            {
                decimal i = decimal.Parse(textBox_Like.Text.Trim());
                plugin.option.QLike = i;
            }
            catch (Exception)
            {
                decimal i = 1;
                plugin.option.QLike = i;
            }
            plugin.option.QAList.Clear();
            for (int i = 0; i < listView_QA.Items.Count; i++)
            {
                string q = listView_QA.Items[i].SubItems[0].Text;
                string a = listView_QA.Items[i].SubItems[1].Text;
                QA qa = new QA();
                qa.Q = q;
                qa.A = a;
                plugin.option.QAList.Add(qa);
            }

            plugin.option.WriteOption();
        }

        private void ListView_QA_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ToolStripMenuItem[] MenuItemList_Trash = new ToolStripMenuItem[2];
                MenuItemList_Trash[0] = new ToolStripMenuItem("新增", null, new EventHandler(ADD_Click));
                MenuItemList_Trash[1] = new ToolStripMenuItem("删除", null, new EventHandler(DEL_Click));

                ContextMenuStrip cms = new ContextMenuStrip();
                cms.Items.AddRange(MenuItemList_Trash);
                this.listview = 3;
                cms.Show(MousePosition.X, MousePosition.Y);
            }
        }

        private void ListView_FK_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ToolStripMenuItem[] MenuItemList_Trash = new ToolStripMenuItem[2];
                MenuItemList_Trash[0] = new ToolStripMenuItem("新增", null, new EventHandler(ADD_Click));
                MenuItemList_Trash[1] = new ToolStripMenuItem("删除", null, new EventHandler(DEL_Click));

                ContextMenuStrip cms = new ContextMenuStrip();
                cms.Items.AddRange(MenuItemList_Trash);
                this.listview = 2;
                cms.Show(MousePosition.X, MousePosition.Y);
            }
        }

        private void ListView_GList_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ToolStripMenuItem[] MenuItemList_Trash = new ToolStripMenuItem[2];
                MenuItemList_Trash[0] = new ToolStripMenuItem("新增", null, new EventHandler(ADD_Click));
                MenuItemList_Trash[1] = new ToolStripMenuItem("删除", null, new EventHandler(DEL_Click));

                ContextMenuStrip cms = new ContextMenuStrip();
                cms.Items.AddRange(MenuItemList_Trash);
                this.listview = 1;
                cms.Show(MousePosition.X, MousePosition.Y);
            }
        }

        private void DEL_Click(object sender, EventArgs e)
        {
            switch (this.listview)
            {
                case 1:
                    {
                        if (listView_GList.SelectedItems.Count < 1)
                            return;
                        foreach (ListViewItem item in listView_GList.SelectedItems)
                        {
                            listView_GList.Items.RemoveAt(item.Index);
                        }
                        break;
                    }
                case 2:
                    {
                        if (listView_FK.SelectedItems.Count < 1)
                            return;
                        foreach (ListViewItem item in listView_FK.SelectedItems)
                        {
                            listView_FK.Items.RemoveAt(item.Index);
                        }
                        break;
                    }
                case 3:
                    {
                        if (listView_QA.SelectedItems.Count < 1)
                            return;
                        foreach (ListViewItem item in listView_QA.SelectedItems)
                        {
                            listView_QA.Items.RemoveAt(item.Index);
                        }
                        break;
                    }
            }
        }

        private void ADD_Click(object sender, EventArgs e)
        {
            switch (this.listview)
            {
                case 1:
                    {
                        Frm_Add add = new Frm_Add(1);
                        if (add.ShowDialog() == DialogResult.OK)
                        {
                            string gift = add.Value1;
                            string mb = add.Value2;
                            if (string.IsNullOrEmpty(gift) || string.IsNullOrEmpty(mb))
                                return;

                            ListViewItem item = listView_GList.Items.Add(gift);
                            item.SubItems.Add(mb);
                        }
                        break;
                    }
                case 2:
                    {
                        Frm_Add add = new Frm_Add(2);
                        if (add.ShowDialog() == DialogResult.OK)
                        {
                            string fk = add.Value2;
                            if (string.IsNullOrEmpty(fk))
                                return;

                            listView_FK.Items.Add(fk);
                        }
                        break;
                    }
                case 3:
                    {
                        Frm_Add add = new Frm_Add(3);
                        if (add.ShowDialog() == DialogResult.OK)
                        {
                            string q = add.Value1;
                            string a = add.Value2;
                            if (string.IsNullOrEmpty(q) || string.IsNullOrEmpty(a))
                                return;

                            ListViewItem item = listView_QA.Items.Add(q);
                            item.SubItems.Add(a);
                        }
                        break;
                    }
            }
        }

        private void button_Bili_Click(object sender, EventArgs e)
        {
            //Frm_Login frm = new Frm_Login();
            //if (frm.ShowDialog() == DialogResult.OK)
            //{
            //    if (string.IsNullOrEmpty(plugin.option.Cookie))
            //        this.button_Bili.Text = "B站帐号登陆";
            //    else
            //        this.button_Bili.Text = "重登B站帐号";
            //}
        }

        private void button_MengbiSetting_Click(object sender, EventArgs e)
        {
            新蒙蔽机器人.plugin.getIns().admin();
        }

        private void Help_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.ijglb.com/chatrobot");
        }
    }
}
