﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace 智能聊天
{
    public class Option
    {
        IniFiles ini = new IniFiles(plugin.iniPath);

        public string User { get; set; }
        public string Pwd { get; set; }

        private string _ACCESS_TOKEN = "";
        public string ACCESS_TOKEN
        {
            get { return _ACCESS_TOKEN; }
            set
            {
                _ACCESS_TOKEN = value;
                ini.WriteString("Base", "ACCESS_TOKEN", _ACCESS_TOKEN);
            }
        }
        private string _REFRESH_TOKEN = "";
        public string REFRESH_TOKEN
        {
            get { return _REFRESH_TOKEN; }
            set
            {
                _REFRESH_TOKEN = value;
                ini.WriteString("Base", "REFRESH_TOKEN", _REFRESH_TOKEN);
            }
        }
        private string _Cookie = "";
        /// <summary>
        /// Bilibili登陆Cookie
        /// </summary>
        public String Cookie
        {
            get { return _Cookie; }
            set
            {
                _Cookie = value;
                ini.WriteString("Base", "Cookie", _Cookie);
            }
        }
        public bool UseCookie { get; set; }
        /// <summary>
        /// 弹幕字数
        /// </summary>
        public Int32 DMZS { get; set; }
        /// <summary>
        /// 登录帐号UID
        /// </summary>
        public String Uid
        {
            get
            {
                try
                {
                    string u = new Regex("DedeUserID=(\\d+);").Match(this.Cookie).Groups[1].Value;
                    return u;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        #region 欢迎老爷...
        /// <summary>
        /// 是否欢迎老爷
        /// </summary>
        public Boolean IsHYLY { get; set; }
        /// <summary>
        /// 欢迎老爷模板
        /// </summary>
        public String LYMB { get; set; }
        #endregion

        #region 图灵聊天...
        /// <summary>
        /// 是否开启图灵聊天
        /// </summary>
        public Boolean IsTL { get; set; }
        /// <summary>
        /// 图灵-ApiKey
        /// </summary>
        public String TLApi { get; set; }
        /// <summary>
        /// 图灵-回避关键字
        /// </summary>
        public String TLKeyWords { get; set; }
        #endregion

        #region 礼物弹幕感谢...
        /// <summary>
        /// 是否开启弹幕感谢
        /// </summary>
        public Boolean IsTKGift { get; set; }
        /// <summary>
        /// 是否开启感谢默认模板
        /// </summary>
        public Boolean IsGDMB { get; set; }
        /// <summary>
        /// 礼物感谢默认模板
        /// </summary>
        public String GDefaultMB { get; set; }
        /// <summary>
        /// 礼物感谢间隔（毫秒）
        /// </summary>
        public Int32 GInterval { get; set; }
        /// <summary>
        /// 礼物感谢模板列表
        /// </summary>
        public List<GiftMB> GMBList { get; set; }
        #endregion

        #region 自动发言...
        /// <summary>
        /// 是否开启自动发言
        /// </summary>
        public Boolean IsAutoFK { get; set; }
        /// <summary>
        /// 自动发言间隔（毫秒）
        /// </summary>
        public Int32 FKInterval { get; set; }
        /// <summary>
        /// 自动发言列表
        /// </summary>
        public List<String> FKList { get; set; }
        #endregion

        #region 智能回答...
        /// <summary>
        /// 是否开启智能回答
        /// </summary>
        public Boolean IsQA { get; set; }
        /// <summary>
        /// 问题相似度
        /// </summary>
        public Decimal QLike { get; set; }
        /// <summary>
        /// QA列表
        /// </summary>
        public List<QA> QAList { get; set; }
        #endregion

        /// <summary>
        /// 读取配置
        /// </summary>
        public void ReadOption()
        {
            this.User = ini.ReadString("Base", "User", "");
            this.Pwd = ini.ReadString("Base", "Pwd", "");
            this._Cookie = ini.ReadString("Base", "Cookie", "");
            this._ACCESS_TOKEN = ini.ReadString("Base", "ACCESS_TOKEN", "");
            this._REFRESH_TOKEN = ini.ReadString("Base", "REFRESH_TOKEN", "");
            this.UseCookie = ini.ReadBool("Base", "UseCookie", false);
            //弹幕字数
            this.DMZS = ini.ReadInteger("Base", "DMZS", 20);

            //弹幕欢迎老爷
            this.IsHYLY = ini.ReadBool("Base", "LY", false);
            this.LYMB = ini.ReadString("LY", "M", "欢迎老爷：%name%进入直播间！");

            //图灵
            this.IsTL = ini.ReadBool("Base", "TL", false);
            this.TLApi = ini.ReadString("TL", "APIKey", "");
            this.TLKeyWords = ini.ReadString("TL", "KeyWords", "点歌/切歌").Replace("/", "|");

            //弹幕感谢
            this.IsTKGift = ini.ReadBool("Base", "TKGift", false);
            this.IsGDMB = ini.ReadBool("TKGift", "Default", true);
            this.GDefaultMB = ini.ReadString("TKGift", "DMould", "感谢%name%赠送的%gift%，么么哒~");
            this.GInterval = ini.ReadInteger("TKGift", "Interval", 5) * 1000;
            this.GMBList.Clear();
            NameValueCollection gl = new NameValueCollection();
            ini.ReadSectionValues("GiftList", gl);
            foreach (string name in gl)
            {
                GiftMB mb = new GiftMB();
                mb.Name = name;
                mb.MB = gl[name];
                this.GMBList.Add(mb);
            }

            //自动发言
            this.IsAutoFK = ini.ReadBool("Base", "AutoFK", false);
            this.FKInterval = ini.ReadInteger("AutoFK", "Interval", 300) * 1000;
            this.FKList.Clear();
            NameValueCollection fkl = new NameValueCollection();
            ini.ReadSectionValues("FKList", fkl);
            foreach (string i in fkl)
            {
                this.FKList.Add(fkl[i]);
            }

            //智能回答
            this.IsQA = ini.ReadBool("Base", "QA", false);
            string like = ini.ReadString("QA", "Like", "1");
            this.QLike = decimal.Parse(like);
            this.QAList.Clear();
            NameValueCollection qal = new NameValueCollection();
            ini.ReadSectionValues("QAList", qal);
            foreach (string q in qal)
            {
                QA qa = new QA();
                qa.Q = q;
                qa.A = qal[q];
                this.QAList.Add(qa);
            }
        }
        /// <summary>
        /// 写入配置
        /// </summary>
        public void WriteOption()
        {
            ini.WriteString("Base", "User", this.User);
            ini.WriteString("Base", "Pwd", this.Pwd);
            ini.WriteString("Base", "Cookie", this.Cookie);
            ini.WriteString("Base", "ACCESS_TOKEN", this.ACCESS_TOKEN);
            ini.WriteString("Base", "REFRESH_TOKEN", this.REFRESH_TOKEN);
            ini.WriteBool("Base", "UseCookie", this.UseCookie);
            //弹幕字数
            ini.WriteInteger("Base", "DMZS", this.DMZS);

            //弹幕欢迎老爷
            ini.WriteBool("Base", "LY", this.IsHYLY);
            ini.WriteString("LY", "M", this.LYMB);

            //图灵
            ini.WriteBool("Base", "TL", this.IsTL);
            ini.WriteString("TL", "APIKey", this.TLApi);
            ini.WriteString("TL", "KeyWords", this.TLKeyWords.Replace("|", "/"));

            //弹幕感谢
            ini.WriteBool("Base", "TKGift", this.IsTKGift);
            ini.WriteBool("TKGift", "Default", this.IsGDMB);
            ini.WriteString("TKGift", "DMould", this.GDefaultMB);
            ini.WriteInteger("TKGift", "Interval", this.GInterval / 1000);
            ini.EraseSection("GiftList");
            foreach (var mb in this.GMBList)
            {
                ini.WriteString("GiftList", mb.Name, mb.MB);
            }

            //自动发言
            ini.WriteBool("Base", "AutoFK", this.IsAutoFK);
            ini.WriteInteger("AutoFK", "Interval", this.FKInterval / 1000);
            ini.EraseSection("FKList");
            int i = 0;
            foreach (var str in this.FKList)
            {
                ini.WriteString("FKList", i.ToString(), str);
                i++;
            }

            //智能回答
            ini.WriteBool("Base", "QA", this.IsQA);
            ini.WriteString("QA", "Like", this.QLike.ToString());
            ini.EraseSection("QAList");
            foreach (var qa in this.QAList)
            {
                ini.WriteString("QAList", qa.Q, qa.A);
            }
        }

    }
    /// <summary>
    /// 礼物感谢模板
    /// </summary>
    public class GiftMB
    {
        /// <summary>
        /// 礼物名
        /// </summary>
        public String Name { get; set; }
        /// <summary>
        /// 对应模板
        /// </summary>
        public String MB { get; set; }
    }
    /// <summary>
    /// QA问答
    /// </summary>
    public class QA
    {
        /// <summary>
        /// 问题
        /// </summary>
        public String Q { get; set; }
        /// <summary>
        /// 回答
        /// </summary>
        public String A { get; set; }
    }
}
