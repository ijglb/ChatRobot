﻿namespace 智能聊天
{
    partial class Frm_Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox_TL = new System.Windows.Forms.GroupBox();
            this.Help = new System.Windows.Forms.LinkLabel();
            this.textBox_KeyWords = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_ApiKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox_TL = new System.Windows.Forms.CheckBox();
            this.groupBox_Gift = new System.Windows.Forms.GroupBox();
            this.textBox_GInterval = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_DMould = new System.Windows.Forms.TextBox();
            this.checkBox_DMould = new System.Windows.Forms.CheckBox();
            this.listView_GList = new System.Windows.Forms.ListView();
            this.GName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.GMould = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.checkBox_TkGift = new System.Windows.Forms.CheckBox();
            this.groupBox_AutoFK = new System.Windows.Forms.GroupBox();
            this.textBox_FKInterval = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.listView_FK = new System.Windows.Forms.ListView();
            this.checkBox_AutoFK = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton_30 = new System.Windows.Forms.RadioButton();
            this.radioButton_20 = new System.Windows.Forms.RadioButton();
            this.groupBox_LY = new System.Windows.Forms.GroupBox();
            this.textBox_LY = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox_LY = new System.Windows.Forms.CheckBox();
            this.groupBox_QA = new System.Windows.Forms.GroupBox();
            this.textBox_Like = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.listView_QA = new System.Windows.Forms.ListView();
            this.Q = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.A = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.checkBox_QA = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button_MengbiSetting = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox_User = new System.Windows.Forms.TextBox();
            this.textBox_Password = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_Cookies = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBox_Cookies = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox_TL.SuspendLayout();
            this.groupBox_Gift.SuspendLayout();
            this.groupBox_AutoFK.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox_LY.SuspendLayout();
            this.groupBox_QA.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox_Cookies);
            this.groupBox1.Controls.Add(this.textBox_Cookies);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.textBox_Password);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBox_User);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(363, 74);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "弹幕发送者";
            // 
            // groupBox_TL
            // 
            this.groupBox_TL.Controls.Add(this.Help);
            this.groupBox_TL.Controls.Add(this.textBox_KeyWords);
            this.groupBox_TL.Controls.Add(this.label2);
            this.groupBox_TL.Controls.Add(this.textBox_ApiKey);
            this.groupBox_TL.Controls.Add(this.label1);
            this.groupBox_TL.Controls.Add(this.checkBox_TL);
            this.groupBox_TL.Location = new System.Drawing.Point(159, 92);
            this.groupBox_TL.Name = "groupBox_TL";
            this.groupBox_TL.Size = new System.Drawing.Size(216, 213);
            this.groupBox_TL.TabIndex = 2;
            this.groupBox_TL.TabStop = false;
            this.groupBox_TL.Text = "图灵机器人（自动聊天）";
            // 
            // Help
            // 
            this.Help.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Help.AutoSize = true;
            this.Help.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Help.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.Help.Location = new System.Drawing.Point(112, 191);
            this.Help.Name = "Help";
            this.Help.Size = new System.Drawing.Size(98, 14);
            this.Help.TabIndex = 5;
            this.Help.TabStop = true;
            this.Help.Text = "插件帮助/说明";
            this.Help.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Help_LinkClicked);
            // 
            // textBox_KeyWords
            // 
            this.textBox_KeyWords.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox_KeyWords.Location = new System.Drawing.Point(6, 110);
            this.textBox_KeyWords.Name = "textBox_KeyWords";
            this.textBox_KeyWords.Size = new System.Drawing.Size(204, 23);
            this.textBox_KeyWords.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(6, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 14);
            this.label2.TabIndex = 3;
            this.label2.Text = "包含下列词则不回复(用/隔开)：";
            // 
            // textBox_ApiKey
            // 
            this.textBox_ApiKey.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox_ApiKey.Location = new System.Drawing.Point(6, 61);
            this.textBox_ApiKey.Name = "textBox_ApiKey";
            this.textBox_ApiKey.Size = new System.Drawing.Size(204, 23);
            this.textBox_ApiKey.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(6, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "APIkey：";
            // 
            // checkBox_TL
            // 
            this.checkBox_TL.AutoSize = true;
            this.checkBox_TL.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox_TL.Location = new System.Drawing.Point(6, 20);
            this.checkBox_TL.Name = "checkBox_TL";
            this.checkBox_TL.Size = new System.Drawing.Size(138, 18);
            this.checkBox_TL.TabIndex = 0;
            this.checkBox_TL.Text = "开启图灵自动聊天";
            this.checkBox_TL.UseVisualStyleBackColor = true;
            // 
            // groupBox_Gift
            // 
            this.groupBox_Gift.Controls.Add(this.textBox_GInterval);
            this.groupBox_Gift.Controls.Add(this.label3);
            this.groupBox_Gift.Controls.Add(this.textBox_DMould);
            this.groupBox_Gift.Controls.Add(this.checkBox_DMould);
            this.groupBox_Gift.Controls.Add(this.listView_GList);
            this.groupBox_Gift.Controls.Add(this.checkBox_TkGift);
            this.groupBox_Gift.Location = new System.Drawing.Point(381, 12);
            this.groupBox_Gift.Name = "groupBox_Gift";
            this.groupBox_Gift.Size = new System.Drawing.Size(216, 293);
            this.groupBox_Gift.TabIndex = 3;
            this.groupBox_Gift.TabStop = false;
            this.groupBox_Gift.Text = "弹幕感谢（收到礼物时）";
            // 
            // textBox_GInterval
            // 
            this.textBox_GInterval.Location = new System.Drawing.Point(117, 264);
            this.textBox_GInterval.Name = "textBox_GInterval";
            this.textBox_GInterval.Size = new System.Drawing.Size(93, 21);
            this.textBox_GInterval.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(6, 267);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 14);
            this.label3.TabIndex = 5;
            this.label3.Text = "答谢间隔(秒)：";
            // 
            // textBox_DMould
            // 
            this.textBox_DMould.Location = new System.Drawing.Point(6, 231);
            this.textBox_DMould.Name = "textBox_DMould";
            this.textBox_DMould.Size = new System.Drawing.Size(204, 21);
            this.textBox_DMould.TabIndex = 4;
            // 
            // checkBox_DMould
            // 
            this.checkBox_DMould.AutoSize = true;
            this.checkBox_DMould.Location = new System.Drawing.Point(6, 209);
            this.checkBox_DMould.Name = "checkBox_DMould";
            this.checkBox_DMould.Size = new System.Drawing.Size(78, 16);
            this.checkBox_DMould.TabIndex = 3;
            this.checkBox_DMould.Text = "默认模板:";
            this.checkBox_DMould.UseVisualStyleBackColor = true;
            // 
            // listView_GList
            // 
            this.listView_GList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.GName,
            this.GMould});
            this.listView_GList.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listView_GList.FullRowSelect = true;
            this.listView_GList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView_GList.Location = new System.Drawing.Point(6, 44);
            this.listView_GList.MultiSelect = false;
            this.listView_GList.Name = "listView_GList";
            this.listView_GList.Size = new System.Drawing.Size(204, 159);
            this.listView_GList.TabIndex = 2;
            this.listView_GList.UseCompatibleStateImageBehavior = false;
            this.listView_GList.View = System.Windows.Forms.View.Details;
            // 
            // GName
            // 
            this.GName.Text = "礼物";
            this.GName.Width = 80;
            // 
            // GMould
            // 
            this.GMould.Text = "感谢模板";
            this.GMould.Width = 200;
            // 
            // checkBox_TkGift
            // 
            this.checkBox_TkGift.AutoSize = true;
            this.checkBox_TkGift.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox_TkGift.Location = new System.Drawing.Point(6, 20);
            this.checkBox_TkGift.Name = "checkBox_TkGift";
            this.checkBox_TkGift.Size = new System.Drawing.Size(110, 18);
            this.checkBox_TkGift.TabIndex = 1;
            this.checkBox_TkGift.Text = "开启弹幕感谢";
            this.checkBox_TkGift.UseVisualStyleBackColor = true;
            // 
            // groupBox_AutoFK
            // 
            this.groupBox_AutoFK.Controls.Add(this.textBox_FKInterval);
            this.groupBox_AutoFK.Controls.Add(this.label4);
            this.groupBox_AutoFK.Controls.Add(this.listView_FK);
            this.groupBox_AutoFK.Controls.Add(this.checkBox_AutoFK);
            this.groupBox_AutoFK.Location = new System.Drawing.Point(603, 12);
            this.groupBox_AutoFK.Name = "groupBox_AutoFK";
            this.groupBox_AutoFK.Size = new System.Drawing.Size(216, 293);
            this.groupBox_AutoFK.TabIndex = 4;
            this.groupBox_AutoFK.TabStop = false;
            this.groupBox_AutoFK.Text = "自动发言（随机公告）";
            // 
            // textBox_FKInterval
            // 
            this.textBox_FKInterval.Location = new System.Drawing.Point(6, 264);
            this.textBox_FKInterval.Name = "textBox_FKInterval";
            this.textBox_FKInterval.Size = new System.Drawing.Size(204, 21);
            this.textBox_FKInterval.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(3, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 14);
            this.label4.TabIndex = 7;
            this.label4.Text = "发言间隔（秒）：";
            // 
            // listView_FK
            // 
            this.listView_FK.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listView_FK.FullRowSelect = true;
            this.listView_FK.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView_FK.Location = new System.Drawing.Point(6, 44);
            this.listView_FK.MultiSelect = false;
            this.listView_FK.Name = "listView_FK";
            this.listView_FK.Size = new System.Drawing.Size(204, 188);
            this.listView_FK.TabIndex = 3;
            this.listView_FK.UseCompatibleStateImageBehavior = false;
            this.listView_FK.View = System.Windows.Forms.View.List;
            // 
            // checkBox_AutoFK
            // 
            this.checkBox_AutoFK.AutoSize = true;
            this.checkBox_AutoFK.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox_AutoFK.Location = new System.Drawing.Point(6, 20);
            this.checkBox_AutoFK.Name = "checkBox_AutoFK";
            this.checkBox_AutoFK.Size = new System.Drawing.Size(110, 18);
            this.checkBox_AutoFK.TabIndex = 2;
            this.checkBox_AutoFK.Text = "开启自动发言";
            this.checkBox_AutoFK.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton_30);
            this.groupBox2.Controls.Add(this.radioButton_20);
            this.groupBox2.Location = new System.Drawing.Point(12, 190);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(141, 55);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "弹幕字数";
            // 
            // radioButton_30
            // 
            this.radioButton_30.AutoSize = true;
            this.radioButton_30.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radioButton_30.Location = new System.Drawing.Point(66, 21);
            this.radioButton_30.Name = "radioButton_30";
            this.radioButton_30.Size = new System.Drawing.Size(53, 18);
            this.radioButton_30.TabIndex = 1;
            this.radioButton_30.TabStop = true;
            this.radioButton_30.Text = "30字";
            this.radioButton_30.UseVisualStyleBackColor = true;
            // 
            // radioButton_20
            // 
            this.radioButton_20.AutoSize = true;
            this.radioButton_20.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.radioButton_20.Location = new System.Drawing.Point(7, 21);
            this.radioButton_20.Name = "radioButton_20";
            this.radioButton_20.Size = new System.Drawing.Size(53, 18);
            this.radioButton_20.TabIndex = 0;
            this.radioButton_20.TabStop = true;
            this.radioButton_20.Text = "20字";
            this.radioButton_20.UseVisualStyleBackColor = true;
            // 
            // groupBox_LY
            // 
            this.groupBox_LY.Controls.Add(this.textBox_LY);
            this.groupBox_LY.Controls.Add(this.label5);
            this.groupBox_LY.Controls.Add(this.checkBox_LY);
            this.groupBox_LY.Location = new System.Drawing.Point(12, 92);
            this.groupBox_LY.Name = "groupBox_LY";
            this.groupBox_LY.Size = new System.Drawing.Size(141, 93);
            this.groupBox_LY.TabIndex = 6;
            this.groupBox_LY.TabStop = false;
            this.groupBox_LY.Text = "弹幕欢迎老爷进房";
            // 
            // textBox_LY
            // 
            this.textBox_LY.Location = new System.Drawing.Point(9, 62);
            this.textBox_LY.Name = "textBox_LY";
            this.textBox_LY.Size = new System.Drawing.Size(123, 21);
            this.textBox_LY.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(6, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 1;
            this.label5.Text = "模板：";
            // 
            // checkBox_LY
            // 
            this.checkBox_LY.AutoSize = true;
            this.checkBox_LY.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox_LY.Location = new System.Drawing.Point(7, 21);
            this.checkBox_LY.Name = "checkBox_LY";
            this.checkBox_LY.Size = new System.Drawing.Size(110, 18);
            this.checkBox_LY.TabIndex = 0;
            this.checkBox_LY.Text = "开启欢迎老爷";
            this.checkBox_LY.UseVisualStyleBackColor = true;
            // 
            // groupBox_QA
            // 
            this.groupBox_QA.Controls.Add(this.textBox_Like);
            this.groupBox_QA.Controls.Add(this.label6);
            this.groupBox_QA.Controls.Add(this.listView_QA);
            this.groupBox_QA.Controls.Add(this.checkBox_QA);
            this.groupBox_QA.Location = new System.Drawing.Point(825, 12);
            this.groupBox_QA.Name = "groupBox_QA";
            this.groupBox_QA.Size = new System.Drawing.Size(216, 293);
            this.groupBox_QA.TabIndex = 7;
            this.groupBox_QA.TabStop = false;
            this.groupBox_QA.Text = "智能回答";
            // 
            // textBox_Like
            // 
            this.textBox_Like.Location = new System.Drawing.Point(111, 263);
            this.textBox_Like.Name = "textBox_Like";
            this.textBox_Like.Size = new System.Drawing.Size(99, 21);
            this.textBox_Like.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(6, 267);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 14);
            this.label6.TabIndex = 4;
            this.label6.Text = "相似度(0-1)：";
            // 
            // listView_QA
            // 
            this.listView_QA.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Q,
            this.A});
            this.listView_QA.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listView_QA.FullRowSelect = true;
            this.listView_QA.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView_QA.Location = new System.Drawing.Point(6, 44);
            this.listView_QA.MultiSelect = false;
            this.listView_QA.Name = "listView_QA";
            this.listView_QA.Size = new System.Drawing.Size(204, 208);
            this.listView_QA.TabIndex = 3;
            this.listView_QA.UseCompatibleStateImageBehavior = false;
            this.listView_QA.View = System.Windows.Forms.View.Details;
            // 
            // Q
            // 
            this.Q.Text = "问题";
            this.Q.Width = 80;
            // 
            // A
            // 
            this.A.Text = "回答";
            this.A.Width = 200;
            // 
            // checkBox_QA
            // 
            this.checkBox_QA.AutoSize = true;
            this.checkBox_QA.Font = new System.Drawing.Font("宋体", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.checkBox_QA.Location = new System.Drawing.Point(6, 20);
            this.checkBox_QA.Name = "checkBox_QA";
            this.checkBox_QA.Size = new System.Drawing.Size(110, 18);
            this.checkBox_QA.TabIndex = 2;
            this.checkBox_QA.Text = "开启智能回答";
            this.checkBox_QA.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button_MengbiSetting);
            this.groupBox3.Location = new System.Drawing.Point(12, 251);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(141, 54);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "语音朗读";
            // 
            // button_MengbiSetting
            // 
            this.button_MengbiSetting.Location = new System.Drawing.Point(10, 22);
            this.button_MengbiSetting.Name = "button_MengbiSetting";
            this.button_MengbiSetting.Size = new System.Drawing.Size(125, 23);
            this.button_MengbiSetting.TabIndex = 1;
            this.button_MengbiSetting.Text = "设置...";
            this.button_MengbiSetting.UseVisualStyleBackColor = true;
            this.button_MengbiSetting.Click += new System.EventHandler(this.button_MengbiSetting_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "帐号";
            // 
            // textBox_User
            // 
            this.textBox_User.Location = new System.Drawing.Point(45, 17);
            this.textBox_User.Name = "textBox_User";
            this.textBox_User.Size = new System.Drawing.Size(140, 21);
            this.textBox_User.TabIndex = 1;
            // 
            // textBox_Password
            // 
            this.textBox_Password.Location = new System.Drawing.Point(226, 17);
            this.textBox_Password.Name = "textBox_Password";
            this.textBox_Password.Size = new System.Drawing.Size(131, 21);
            this.textBox_Password.TabIndex = 3;
            this.textBox_Password.UseSystemPasswordChar = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(191, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 2;
            this.label8.Text = "密码";
            // 
            // textBox_Cookies
            // 
            this.textBox_Cookies.Location = new System.Drawing.Point(63, 44);
            this.textBox_Cookies.Name = "textBox_Cookies";
            this.textBox_Cookies.Size = new System.Drawing.Size(157, 21);
            this.textBox_Cookies.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 12);
            this.label9.TabIndex = 4;
            this.label9.Text = "Cookies";
            // 
            // checkBox_Cookies
            // 
            this.checkBox_Cookies.AutoSize = true;
            this.checkBox_Cookies.Location = new System.Drawing.Point(234, 47);
            this.checkBox_Cookies.Name = "checkBox_Cookies";
            this.checkBox_Cookies.Size = new System.Drawing.Size(114, 16);
            this.checkBox_Cookies.TabIndex = 6;
            this.checkBox_Cookies.Text = "使用Cookies登录";
            this.checkBox_Cookies.UseVisualStyleBackColor = true;
            // 
            // Frm_Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1052, 317);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox_QA);
            this.Controls.Add(this.groupBox_LY);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox_AutoFK);
            this.Controls.Add(this.groupBox_Gift);
            this.Controls.Add(this.groupBox_TL);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_Admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "智能聊天-设置中心";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Admin_FormClosing);
            this.Load += new System.EventHandler(this.Frm_Admin_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox_TL.ResumeLayout(false);
            this.groupBox_TL.PerformLayout();
            this.groupBox_Gift.ResumeLayout(false);
            this.groupBox_Gift.PerformLayout();
            this.groupBox_AutoFK.ResumeLayout(false);
            this.groupBox_AutoFK.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox_LY.ResumeLayout(false);
            this.groupBox_LY.PerformLayout();
            this.groupBox_QA.ResumeLayout(false);
            this.groupBox_QA.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox_TL;
        private System.Windows.Forms.TextBox textBox_KeyWords;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_ApiKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox_TL;
        private System.Windows.Forms.GroupBox groupBox_Gift;
        private System.Windows.Forms.TextBox textBox_GInterval;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_DMould;
        private System.Windows.Forms.CheckBox checkBox_DMould;
        private System.Windows.Forms.ListView listView_GList;
        private System.Windows.Forms.ColumnHeader GName;
        private System.Windows.Forms.ColumnHeader GMould;
        private System.Windows.Forms.CheckBox checkBox_TkGift;
        private System.Windows.Forms.GroupBox groupBox_AutoFK;
        private System.Windows.Forms.CheckBox checkBox_AutoFK;
        private System.Windows.Forms.ListView listView_FK;
        private System.Windows.Forms.TextBox textBox_FKInterval;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton_30;
        private System.Windows.Forms.RadioButton radioButton_20;
        private System.Windows.Forms.GroupBox groupBox_LY;
        private System.Windows.Forms.CheckBox checkBox_LY;
        private System.Windows.Forms.TextBox textBox_LY;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox_QA;
        private System.Windows.Forms.ListView listView_QA;
        private System.Windows.Forms.CheckBox checkBox_QA;
        private System.Windows.Forms.TextBox textBox_Like;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ColumnHeader Q;
        private System.Windows.Forms.ColumnHeader A;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button_MengbiSetting;
        private System.Windows.Forms.LinkLabel Help;
        private System.Windows.Forms.TextBox textBox_Password;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_User;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBox_Cookies;
        private System.Windows.Forms.TextBox textBox_Cookies;
        private System.Windows.Forms.Label label9;
    }
}