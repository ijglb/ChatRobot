﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace 智能聊天
{
    public static class Operate
    {
        /// <summary>
        /// 不知道干嘛的
        /// </summary>
        public static string Rnd { get; set; }
        /// <summary>
        /// 连接的房间号
        /// </summary>
        public static string RoomID { get; set; }
        /// <summary>
        /// 礼物感谢集
        /// </summary>
        public static List<Gift> GList = new List<Gift>();

        public static bool allow = true;
        
        /// <summary>
        /// 获取RND
        /// </summary>
        public static void GetRnd()
        {
            long d = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1).ToUniversalTime()).TotalMilliseconds;
            Rnd = Math.Floor(d / 1e3).ToString();
        }
        /// <summary>
        /// 发送弹幕
        /// </summary>
        /// <param name="msg"></param>
        public static void sendDM(string msg)
        {
            if (msg.Length > plugin.option.DMZS)//B站弹幕限制20或30字，分段发送
            {
                int j = 0;
                int m = msg.Length / plugin.option.DMZS;
                for (int i = 0; i <= m; i++)
                {
                    if (i == m)
                    {
                        int y = msg.Length - plugin.option.DMZS * m;
                        string s = msg.Substring(j, y);
                        DM(s);
                    }
                    else
                    {
                        string s = msg.Substring(j, plugin.option.DMZS);
                        DM(s);
                    }
                    System.Threading.Thread.Sleep(700);
                    j = plugin.option.DMZS + plugin.option.DMZS * i;
                }
            }
            else
            {
                DM(msg);
                System.Threading.Thread.Sleep(700);
            }
        }

        /// <summary>
        /// 调用图灵
        /// </summary>
        /// <param name="info">信息</param>
        /// <param name="uid">用户标识</param>
        /// <returns></returns>
        public static string sendTL(string info, string uid)
        {
            if (!new Regex(plugin.option.TLKeyWords).IsMatch(info) && uid != plugin.option.Uid)
            {
                try
                {
                    string msg;
                    TLJson json = new TLJson() { info = info, userid = uid };
                    string postString = JsonConvert.SerializeObject(json);
                    byte[] postData = Encoding.UTF8.GetBytes(postString);
                    string url = "http://www.tuling123.com/openapi/api";
                    WebClient webClient = new WebClient();
                    //webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
                    webClient.Headers.Add("Content-Type", "application/json;charset=utf-8");
                    byte[] responseData = webClient.UploadData(url, "POST", postData);
                    string reString = Encoding.UTF8.GetString(responseData);
                    JObject reJson = (JObject)JsonConvert.DeserializeObject(reString);
                    string code = reJson["code"].ToString();
                    string text = reJson["text"].ToString();
                    if (code.Contains("100000"))
                        msg = text;
                    else
                        msg = "图灵出错：" + text;
                    return msg;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }
        /// <summary>
        /// 收到礼物
        /// </summary>
        /// <param name="text"></param>
        public static void ReceiveGift(string text)
        {
            Regex reg = new Regex("收到\\s{1}(.+?)\\s{1}赠送了(\\d+)个(.+?)，谢谢！");
            Match match = reg.Match(text);
            string name = match.Groups[1].Value;
            int count = Convert.ToInt32(match.Groups[2].Value);
            string gift = match.Groups[3].Value;

            var exist = GList.Where(m => m.UserName == name && m.GiftName == gift).SingleOrDefault();
            if (exist != null)
                exist.Count = exist.Count + count;
            else
                GList.Add(new Gift() { UserName = name, GiftName = gift, Count = count });
        }
        /// <summary>
        /// 礼物感谢
        /// </summary>
        public static void TKGift()
        {
            allow = false;
            foreach (var gift in GList)
            {
                string msg = "";
                string mb = plugin.option.GMBList.Where(m => m.Name == gift.GiftName).Select(m => m.MB).Take(1).SingleOrDefault();
                if (!string.IsNullOrEmpty(mb))
                {
                    msg = mb.Replace("%name%", gift.UserName).Replace("%gift%", gift.GiftName).Replace("%count%", gift.Count.ToString());
                    sendDM(msg);
                }
                else
                {
                    if (plugin.option.IsGDMB)
                    {
                        mb = plugin.option.GDefaultMB;
                        msg = mb.Replace("%name%", gift.UserName).Replace("%gift%", gift.GiftName).Replace("%count%", gift.Count.ToString());
                        sendDM(msg);
                    }
                }
            }

            GList.Clear();
            allow = true;
        }
        /// <summary>
        /// 智能回答
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string FindQA(string msg)
        {
            foreach (var qa in plugin.option.QAList)
            {
                decimal like = new LevenshteinDistance().LevenshteinDistancePercent(msg, qa.Q);
                if (like >= plugin.option.QLike)
                    return qa.A;
                else
                    continue;
            }
            return null;
        }
        /// <summary>
        /// 欢迎老爷
        /// </summary>
        /// <param name="text"></param>
        public static void HYLY(string text)
        {
            Regex reg = new Regex("欢迎老爷：(.+?)进入直播间！");
            Match match = reg.Match(text);
            string name = match.Groups[1].Value;

            string mb = plugin.option.LYMB;
            string msg = mb.Replace("%name%", name);
            sendDM(msg);
        }
        /// <summary>
        /// 自动发言
        /// </summary>
        public static void FK()
        {
            if (plugin.option.FKList.Count > 0)
            {
                int i = new Random().Next(0, plugin.option.FKList.Count);
                sendDM(plugin.option.FKList[i]);
            }
        }

        #region 私有...
        /// <summary>
        /// 弹幕
        /// </summary>
        /// <param name="msg"></param>
        private static void DM(string msg)
        {
            string postString = "color=16777215&fontsize=25&mode=1&msg=" + msg + "&rnd=" + Rnd + "&roomid=" + RoomID;
            //MessageBox.Show(postString);
            byte[] postData = Encoding.UTF8.GetBytes(postString);
            string url = "http://live.bilibili.com/msg/send";
            WebClient webClient = new WebClient();
            webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            webClient.Headers.Add("Cookie", plugin.option.Cookie);
            try
            {
                //byte[] responseData = webClient.UploadData(url, "POST", postData);
                webClient.UploadData(url, "POST", postData);
            }
            catch
            {
                return;
            }
        }
        #endregion
    }

    public class TLJson
    {
        /// <summary>
        /// 图灵ApiKey
        /// </summary>
        public string key { get { return plugin.option.TLApi; } }
        /// <summary>
        /// 发送内容
        /// </summary>
        public string info { get; set; }

        public string userid { get; set; }
    }

    public class Gift
    {
        public string UserName { get; set; }

        public string GiftName { get; set; }

        public int Count { get; set; }
    }
}
