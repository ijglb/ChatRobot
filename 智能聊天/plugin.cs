﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace 智能聊天
{
    public class plugin//<--本类名不能改，插件入口
    {
        public static string iniPath = Application.StartupPath + @"\Addons\智能聊天.ini";
        public static Option option;

        private bool Tking = false;
        private System.Timers.Timer autoFK = new System.Timers.Timer();

        public plugin()
        {

        }

        #region UP主助手插件主事件。
        /// <summary>
        /// 软件主界面打开后触发start
        /// </summary>
        public void start()
        {
            BiliBili.pluginInstance = this;
            if (option == null)
            {
                option = new Option();
                option.FKList = new List<string>();
                option.GMBList = new List<GiftMB>();
                option.QAList = new List<QA>();
            }
            option.ReadOption();

            autoFK.Interval = option.FKInterval;
            autoFK.Enabled = false;
            autoFK.Elapsed += AutoFK_Elapsed;
            新蒙蔽机器人.plugin.getIns(this).start();
            
        }

        private void AutoFK_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (option.IsAutoFK)
            {
                Operate.FK();
            }
            if (autoFK.Interval != option.FKInterval)
                autoFK.Interval = option.FKInterval;
        }

        /// <summary>
        /// 软件关闭时触发stop
        /// </summary>
        public void stop()
        {
            新蒙蔽机器人.plugin.getIns().stop();
        }

        /// <summary>
        /// 弹幕来了触发dmshow
        /// </summary>
        public void dmshow(string danmuInfo)
        {
            DanmuInfo o = TransToEntity(danmuInfo);
            Task.Run(new Action(()=>{
                if (o.roomid != null && Operate.RoomID != o.roomid)
                    Operate.RoomID = o.roomid;
                if (string.IsNullOrEmpty(Operate.Rnd))
                    Operate.GetRnd();
                if (option.IsAutoFK && !autoFK.Enabled)
                    autoFK.Enabled = true;

                string msg = o.logtext;
                //string user = o.nickname;
                if (!option.UseCookie)
                    BiliBili.Work();

                if (option.IsTKGift && Operate.allow)
                {
                    if (!Tking)
                    {
                        Tking = true;
                        Thread td = new Thread(new ThreadStart(() => {
                            Thread.Sleep(option.GInterval);
                            Operate.TKGift();
                            Tking = false;

                        }));
                        td.Start();
                    }
                    if (o.type == "gift")
                        Operate.ReceiveGift(msg);
                }

                if (option.IsHYLY)
                {
                    if (o.type == "wellcom")
                        Operate.HYLY(msg);
                }

                if (option.IsQA || option.IsTL)
                {
                    if (option.IsQA)
                    {
                        string res = Operate.FindQA(msg);
                        if (!string.IsNullOrEmpty(res))
                        {
                            Operate.sendDM(res);
                        }
                        else
                        {
                            if (option.IsTL && o.type != "gift" && o.type != "wellcom")
                            {
                                //Regex reg = new Regex("bilibili_uid=(\\d+);");
                                //Match match = reg.Match(o.extra_info);
                                //string userID = match.Groups[1].Value;
                                string userID = o.bilibiliuid;
                                res = Operate.sendTL(msg, userID);
                                if (!string.IsNullOrEmpty(res))
                                {
                                    if (res.Contains("图灵出错："))
                                        SendMSG(res);
                                    else
                                        Operate.sendDM(res);
                                }
                            }
                        }
                    }
                    else if (o.member != 1 && o.type != "gift" && o.type != "wellcom")
                    {
                        //Regex reg = new Regex("bilibili_uid=(\\d+);");
                        //Match match = reg.Match(extra_info);
                        //string userID = match.Groups[1].Value;
                        string userID = o.bilibiliuid;
                        string res = Operate.sendTL(msg, userID);
                        if (!string.IsNullOrEmpty(res))
                        {
                            if (res.Contains("图灵出错："))
                                SendMSG(res);
                            else
                                Operate.sendDM(res);
                        }
                    }
                }

                新蒙蔽机器人.plugin.getIns().dmshow(o);
            }));
            
        }

        /// <summary>
        /// 主程序会分享一些行为信息给插件，方便插件开发者知道主程序在干什么
        /// P.S.新的特性都会发送记录到这里
        /// </summary>
        public void ActionLog(string danmuInfo)
        {
            DanmuInfo o = TransToEntity(danmuInfo);

            新蒙蔽机器人.plugin.getIns().ActionLog(o);
        }

        /// <summary>
        /// 在插件管理器双击插件会触发admin
        /// </summary>
        public void admin()
        {
            if (option == null)
            {
                option = new Option();
                option.FKList = new List<string>();
                option.GMBList = new List<GiftMB>();
                option.QAList = new List<QA>();
            }

            new Frm_Admin().Show();
        }
        #endregion

        #region 核心通讯
        public event EventHandler SMSG;
        public void SendMSG(string text, bool action = false)
        {
            string _text = "{dl_title}";
            if (!action)
                text = "智能聊天插件:" + text;
            if (action)
            {
                _text = "{action}";
            }
            SMSG(new object[] { _text + text }, null);
        }

        public DanmuInfo TransToEntity(string danmuInfo)
        {
            try
            {
                XmlSerializer xmls = new XmlSerializer(typeof(DanmuInfo));
                return (DanmuInfo)xmls.Deserialize(new StringReader(danmuInfo));
            }
            catch (Exception)
            {
                return null;
            }

        }
        #endregion
    }

    #region UP主助手实体类    
    [Serializable]
    public class DanmuInfo :ICloneable
    {
        public string logtext { get; set; }
        [Obsolete]
        public string uid { get; set; }
        public string bilibiliuid { get; set; }
        public string nickname { get; set; }
        public string datetime { get; set; }
        public int admin { get; set; }
        public int member { get; set; }
        public int vip { get; set; }
        public string roomid { get; set; }
        public string extra_info { get; set; }
        public string type { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
            
        }
    }
    #endregion
}
