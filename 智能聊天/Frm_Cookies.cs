﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 智能聊天
{
    public partial class Frm_Cookies : Form
    {
        public Frm_Cookies()
        {
            InitializeComponent();
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            string cookie = this.textBox.Text.Trim();
            plugin.option.Cookie = cookie;

            IniFiles ini = new IniFiles(plugin.iniPath);
            ini.WriteString("Base", "Cookie", cookie);
            this.DialogResult = DialogResult.OK;
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void Frm_Cookies_Load(object sender, EventArgs e)
        {
            this.textBox.Text = plugin.option.Cookie;
        }
    }
}
