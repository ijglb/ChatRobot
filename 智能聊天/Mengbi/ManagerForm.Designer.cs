﻿using System;

namespace 新蒙蔽机器人
{
    partial class ManagerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.NoGiftcheckBox = new System.Windows.Forms.CheckBox();
            this.NoNamecheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.unpackButton = new System.Windows.Forms.Button();
            this.packButton = new System.Windows.Forms.Button();
            this.filterCheckBox1 = new System.Windows.Forms.CheckBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MainContentMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addVoice = new System.Windows.Forms.ToolStripMenuItem();
            this.rmVoice = new System.Windows.Forms.ToolStripMenuItem();
            this.mdVoicePath = new System.Windows.Forms.ToolStripMenuItem();
            this.EnablecheckBox1 = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.DisableCheckBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.MainContentMenu.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DisableCheckBox1);
            this.groupBox1.Controls.Add(this.NoGiftcheckBox);
            this.groupBox1.Controls.Add(this.NoNamecheckBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(315, 169);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "机器人：";
            // 
            // NoGiftcheckBox
            // 
            this.NoGiftcheckBox.AutoSize = true;
            this.NoGiftcheckBox.Location = new System.Drawing.Point(126, 20);
            this.NoGiftcheckBox.Name = "NoGiftcheckBox";
            this.NoGiftcheckBox.Size = new System.Drawing.Size(84, 16);
            this.NoGiftcheckBox.TabIndex = 1;
            this.NoGiftcheckBox.Text = "不读出礼物";
            this.NoGiftcheckBox.UseVisualStyleBackColor = true;
            // 
            // NoNamecheckBox
            // 
            this.NoNamecheckBox.AutoSize = true;
            this.NoNamecheckBox.Location = new System.Drawing.Point(12, 20);
            this.NoNamecheckBox.Name = "NoNamecheckBox";
            this.NoNamecheckBox.Size = new System.Drawing.Size(108, 16);
            this.NoNamecheckBox.TabIndex = 0;
            this.NoNamecheckBox.Text = "不读出客户名字";
            this.NoNamecheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.unpackButton);
            this.groupBox2.Controls.Add(this.packButton);
            this.groupBox2.Controls.Add(this.filterCheckBox1);
            this.groupBox2.Controls.Add(this.listView1);
            this.groupBox2.Controls.Add(this.EnablecheckBox1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 169);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(315, 213);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "增强版道具娘：";
            // 
            // unpackButton
            // 
            this.unpackButton.Location = new System.Drawing.Point(252, 16);
            this.unpackButton.Name = "unpackButton";
            this.unpackButton.Size = new System.Drawing.Size(42, 23);
            this.unpackButton.TabIndex = 4;
            this.unpackButton.Text = "读包";
            this.unpackButton.UseVisualStyleBackColor = true;
            this.unpackButton.Click += new System.EventHandler(this.unpackButton_Click);
            // 
            // packButton
            // 
            this.packButton.Location = new System.Drawing.Point(204, 16);
            this.packButton.Name = "packButton";
            this.packButton.Size = new System.Drawing.Size(42, 23);
            this.packButton.TabIndex = 3;
            this.packButton.Text = "打包";
            this.packButton.UseVisualStyleBackColor = true;
            this.packButton.Click += new System.EventHandler(this.packButton_Click);
            // 
            // filterCheckBox1
            // 
            this.filterCheckBox1.AutoSize = true;
            this.filterCheckBox1.Location = new System.Drawing.Point(114, 20);
            this.filterCheckBox1.Name = "filterCheckBox1";
            this.filterCheckBox1.Size = new System.Drawing.Size(84, 16);
            this.filterCheckBox1.TabIndex = 2;
            this.filterCheckBox1.Text = "刷弹幕过滤";
            this.filterCheckBox1.UseVisualStyleBackColor = true;
            this.filterCheckBox1.CheckedChanged += new System.EventHandler(this.filterCheckBox1_CheckedChanged);
            // 
            // listView1
            // 
            this.listView1.CheckBoxes = true;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listView1.ContextMenuStrip = this.MainContentMenu;
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(12, 42);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(291, 163);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listView1_ItemChecked);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "礼物名称";
            this.columnHeader1.Width = 70;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "音效名";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "路径";
            this.columnHeader3.Width = 120;
            // 
            // MainContentMenu
            // 
            this.MainContentMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addVoice,
            this.rmVoice,
            this.mdVoicePath});
            this.MainContentMenu.Name = "MainContentMenu";
            this.MainContentMenu.Size = new System.Drawing.Size(149, 70);
            // 
            // addVoice
            // 
            this.addVoice.Name = "addVoice";
            this.addVoice.Size = new System.Drawing.Size(148, 22);
            this.addVoice.Text = "添加音效";
            // 
            // rmVoice
            // 
            this.rmVoice.Name = "rmVoice";
            this.rmVoice.Size = new System.Drawing.Size(148, 22);
            this.rmVoice.Text = "删除音效";
            this.rmVoice.Click += new System.EventHandler(this.rmVoice_Click);
            // 
            // mdVoicePath
            // 
            this.mdVoicePath.Name = "mdVoicePath";
            this.mdVoicePath.Size = new System.Drawing.Size(148, 22);
            this.mdVoicePath.Text = "更换音频路径";
            this.mdVoicePath.Click += new System.EventHandler(this.mdVoicePath_Click);
            // 
            // EnablecheckBox1
            // 
            this.EnablecheckBox1.AutoSize = true;
            this.EnablecheckBox1.Location = new System.Drawing.Point(12, 20);
            this.EnablecheckBox1.Name = "EnablecheckBox1";
            this.EnablecheckBox1.Size = new System.Drawing.Size(96, 16);
            this.EnablecheckBox1.TabIndex = 0;
            this.EnablecheckBox1.Text = "启用特殊音效";
            this.EnablecheckBox1.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 380);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(315, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // DisableCheckBox1
            // 
            this.DisableCheckBox1.AutoSize = true;
            this.DisableCheckBox1.Location = new System.Drawing.Point(216, 20);
            this.DisableCheckBox1.Name = "DisableCheckBox1";
            this.DisableCheckBox1.Size = new System.Drawing.Size(72, 16);
            this.DisableCheckBox1.TabIndex = 2;
            this.DisableCheckBox1.Text = "禁用语音";
            this.DisableCheckBox1.UseVisualStyleBackColor = true;
            // 
            // ManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(315, 402);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ManagerForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "语音调教";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ManagerForm_FormClosing);
            this.Load += new System.EventHandler(this.ManagerForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.MainContentMenu.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox NoGiftcheckBox;
        private System.Windows.Forms.CheckBox NoNamecheckBox;
        private System.Windows.Forms.CheckBox EnablecheckBox1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ContextMenuStrip MainContentMenu;
        private System.Windows.Forms.ToolStripMenuItem addVoice;
        private System.Windows.Forms.ToolStripMenuItem rmVoice;
        private System.Windows.Forms.ToolStripMenuItem mdVoicePath;
        public System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.CheckBox filterCheckBox1;
        private System.Windows.Forms.Button packButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button unpackButton;
        private System.Windows.Forms.CheckBox DisableCheckBox1;
    }
}