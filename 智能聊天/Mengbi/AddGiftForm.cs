﻿using System;
using System.Windows.Forms;

namespace 新蒙蔽机器人
{
    public partial class AddGiftForm : Form
    {
        ManagerForm parent;
        public AddGiftForm(ManagerForm parent)
        {
            InitializeComponent();
            this.parent = parent;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Mp3|*.mp3|Wave文件(*.wav)|*.wav|WindowsMedia(*.wma)|*.wma|AAC文件(*.aac)|*.aac";
            if (parent.listView1.FindItemWithText(giftNameComboBox1.Text)!=null)
            {
                MessageBox.Show(this, "这个礼物你已经添加过了。", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (string.IsNullOrWhiteSpace(giftNameComboBox1.Text))
            {
                MessageBox.Show(this,"噗，您没有输入礼物的名称哦。","错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                
                if (new System.IO.FileInfo(ofd.FileName).Length>6291456)
                {
                    MessageBox.Show(this, "哎~文件太大了喂！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                parent.bind_list.Add(new VoiceItem { name = giftNameComboBox1.Text, tag = ofd.FileName.Substring(ofd.FileName.LastIndexOf("\\")+1), path = ofd.FileName, status =1});
                Close();
            }
        }
    }
}
