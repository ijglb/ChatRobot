﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace 新蒙蔽机器人
{
    class Bina
    {
        public static void BinSerialize<T>(T o, Stream stream)
        {
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, o);
                stream.Flush();
                stream.Close();
            }
            catch (Exception) { }
        }
        public static T BinDeSerialize<T>(Stream filestream)
        {
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                T o = (T)formatter.Deserialize(filestream);
                return o;
            }
            catch (Exception)
            {
            }
            return default(T);
        }
        public static void Serialize<T>(T o, Stream stream)
        {
            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(T));
                formatter.Serialize(stream, o);
                stream.Flush();
                stream.Close();
            }
            catch (Exception) { }
        }
        public static T DeSerialize<T>(Stream filestream)
        {
            try
            {
                XmlSerializer formatter = new XmlSerializer(typeof(T));
                T o = (T)formatter.Deserialize(filestream);
                return o;
            }
            catch (Exception)
            {
            }
            return default(T);
        }
    }
}
