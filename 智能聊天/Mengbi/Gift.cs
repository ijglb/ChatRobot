﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Timers;

namespace 新蒙蔽机器人
{
    public class Gift
    {
        public Gift(){
            filter_clock.Interval = 5000;//线性过滤时间为五秒
            filter_clock.Elapsed += Filter_clock_Elapsed;
        }
        public bool need_filter;
        private bool enable_voice;
        public List<IPackge> packges = new List<IPackge>();
        private Timer filter_clock = new Timer();
        private bool need_layer;

        public bool enableVoice
        {
            get { return enable_voice; }
            set { enable_voice = value; }
        }
        public bool needLayer
        {
            get { return need_layer; }
            set { need_layer = value; }
        }
        /// <summary>
        /// 查找录音
        /// </summary>
        /// <param name="gift_name">礼物名称</param>
        /// <returns></returns>
        public string play(String gift_name)
        {
            string path = "";
            if (enableVoice)
            {
                Match res = Regex.Match(gift_name, ".*赠送了([0-9\\d*])个(.*)，");
                bool canplay = true;
                //过滤
                if (need_filter)
                {
                    if (need_layer)
                    {
                        canplay = canPlay(res.Groups[2].Value);
                    }
                    else
                    {
                        canplay = canPlay();
                    }
                }
                //应用过滤
                if (!canplay)
                {
                    return path;
                }
                //多包操作，如果多个包都有音效则选第一个包中的音效，如果第一个包没有则找下一个包中的音效
                foreach (IPackge item in packges)
                {
                    if (res.Groups.Count<1)
                    {
                        break;
                    }
                    path = item.getPath(res.Groups[2].Value);//查路径
                    if (!string.IsNullOrEmpty(path))
                    {
                        break;
                    }
                }
            }
            return path;
        }
        /// <summary>
        /// 线性过滤器
        /// </summary>
        /// <returns></returns>
        public bool canPlay()
        {
            if (!filter_clock.Enabled)
            {
                filter_clock.Start();
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 分层过滤器
        /// </summary>
        /// <param name="gift_name">礼物名称</param>
        /// <returns></returns>
        public bool canPlay(string gift_name)
        {

            return canPlay();
        }
        private void Filter_clock_Elapsed(object sender, ElapsedEventArgs e)
        {
            filter_clock.Enabled = false;
        }
    }
}
