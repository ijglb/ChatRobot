﻿using System.Collections.Generic;

namespace 新蒙蔽机器人
{
    public interface IPackge
    {
        string name { get; set; }

        string getVoice(string gift_name);
        bool setVoiceList(List<VoiceItem> e);
        List<VoiceItem> getVoiceList();
        bool setVoiceToPackge();
        bool pack();
        bool unpack();
        void setPath(string v);
        string getPath(string value);
    }
}
