﻿using System.Collections.Generic;

namespace 新蒙蔽机器人
{
    public abstract class TTS
    {
        private bool enable_spk;
        private bool disable_spk_gift;
        private bool no_name;
        protected double voice_speed;
        protected double voice_vol;
        protected Queue<string> spk_queue = new Queue<string>();

        public bool enableSpk
        {
            get { return enable_spk; }
            set { enable_spk = value; }
        }

        public bool disableSpkGift
        {
            get { return disable_spk_gift; }
            set { disable_spk_gift = value; }
        }
        public bool noName
        {
            get { return no_name; }
            set { no_name = value; }
        }
        public abstract void setVoiceSpeed(double speed);
        public abstract void setVoiceVol(double vol);
        public void addContent(string type,string h_text = "", string u_nickname = "隐藏BOSS")
        {
            string say = "";
            /*是否禁用礼物朗读*/
            if (!enableSpk || (disableSpkGift && type=="gift"))
            {
                return;
            }
            /*应用设置*/
            if (no_name)
            {
                say = h_text;
            }
            else
            {
                if (type=="wellcome")
                {
                    say = "FreeStlye系统：" + h_text;
                }
                else
                {
                    say = u_nickname + "说" + h_text;
                }
                
            }
            spk_queue.Enqueue(say);
        }
        public abstract void speak();
        public abstract void run();//呼叫对象播放
        public abstract void stop();
    }
}
