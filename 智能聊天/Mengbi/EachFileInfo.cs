﻿using System;
using System.Collections.Generic;

namespace 新蒙蔽机器人
{
    [Serializable]
    public class EachFileInfo
    {
        public EachFileInfo()
        {
            list = new List<Info>();
        }
        public List<Info> list;
        public class Info
        {
            public string name { get; set; }
            public long lenght { get; set; }
            public long offset { get; set; }
            public string path { get; set; }
        }
        
    }
}
