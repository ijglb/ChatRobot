﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace 新蒙蔽机器人
{
    public partial class ManagerForm : Form
    {
        plugin father;
        public List<VoiceItem> bind_list;
        public ManagerForm(plugin pd)
        {
            InitializeComponent();
            father = pd;
            addVoice.Click += AddVoice_Click;
            bind_list = new List<VoiceItem>();

        }

      

        private void ManagerForm_Load(object sender, EventArgs e)
        {
            try
            {
                NoNamecheckBox.Checked = father.tts.noName;
                NoGiftcheckBox.Checked = father.tts.disableSpkGift;
                EnablecheckBox1.Checked = father.gift.enableVoice;
            }
            catch (Exception)
            {
                MessageBox.Show("请检查你目前是否把软件装在了C盘，如果是请以“管理员身份运行”！");
            }
            
            try
            {
                bind_list = father.gift.packges.Find((s) => s.name == "default").getVoiceList();
                RefView();
            }
            catch (Exception)
            {
                MessageBox.Show("由于没有检测到默认的音效包，系统将自动建立音效包并保存！", "音效包：");
            }
        }

        private void ManagerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (father.tts !=null)
            {
                father.tts.noName = NoNamecheckBox.Checked;
                father.tts.disableSpkGift = NoGiftcheckBox.Checked;
                father.tts.enableSpk = !DisableCheckBox1.Checked;
                father.gift.enableVoice = EnablecheckBox1.Checked;
            }
            //更新选中状态
            if (bind_list!=null)
            {
                bind_list.ForEach((s) => {
                    s.status = listView1.FindItemWithText(s.name).Checked ? 1 : 0;
                });
            }

            if (MessageBox.Show(this,"是否立即保存当前的音效包？\n提示：如果你修改了列表内容最好选“是”，否则可能会丢失更改哦！","音效包：", MessageBoxButtons.YesNo)== DialogResult.Yes)
            {
                SavePackage().pack();//保存包然后打包
            }
            else
            {
                SavePackage();//保存
            }
            
        }
        private void AddVoice_Click(object sender, EventArgs e)
        {
            AddGiftForm agf = new AddGiftForm(this);
            agf.ShowDialog();
            RefView();
        }
        private IPackge SavePackage()
        {
            IPackge pkg = new Packge();
            pkg.setVoiceList(bind_list);
            pkg.name = "default";
            pkg.setPath(plugin.BASEDIR+"默认音效包.npkg");
            int idx = father.gift.packges.FindIndex((s) => s.name.Equals(pkg.name));//重复包判断
            if (idx != -1)
            {
                father.gift.packges[idx] = pkg;
            }
            else
            {
                father.gift.packges.Add(pkg);
            }
            return pkg;
        }
        private void RefView()
        {
            try
            {
                this.BeginInvoke(new Action(() => {
                    listView1.Items.Clear();
                    bind_list.ForEach((s) => {
                        ListViewItem current = listView1.Items.Add(s.name);
                        current.SubItems.Add(s.tag);
                        current.SubItems.Add(s.path);
                        current.Checked = s.status == 1 ? true : false;
                    });
                }));
                
            }
            catch (Exception)
            {

            }
        }
        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (listView1.CheckedItems.Count<1)
            {
                return;
            }
            foreach (ListViewItem item in listView1.CheckedItems)
            {
                bind_list[item.Index].status = item.Checked ? 1 : 0;
            }
        }

        private void filterCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            father.gift.need_filter = filterCheckBox1.Checked;
        }

        private void packButton_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "正在保存包...";
            IPackge pkg = SavePackage();
            toolStripStatusLabel1.Text = "正在打包...";
            SaveFileDialog ofd = new SaveFileDialog();
            ofd.Filter = "NPKG文件(*.npkg)|*.npkg";
            ofd.Title = "保存当前的音效包";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                pkg.setPath(ofd.FileName);
                try
                {
                    pkg.pack();
                    toolStripStatusLabel1.Text = "打包完成";
                }
                catch (Exception)
                {
                    toolStripStatusLabel1.Text = "打包失败，文件不存在或者受限";
                }
            }
            else
            {
                toolStripStatusLabel1.Text = "打包失败，没有选择文件";
            }
        }

        private void unpackButton_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "正在解包...";
            IPackge now = null;
            now = father.gift.packges.Find((s) => s.name == "default");
            if (now != null)
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "NPKG文件(*.npkg)|*.npkg";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    now.setPath(ofd.FileName);
                    try
                    {
                        if (now.unpack())
                        {
                            toolStripStatusLabel1.Text = "更新包";
                            bind_list = father.gift.packges.Find((s) => s.name == "default").getVoiceList();
                            RefView();
                            toolStripStatusLabel1.Text = "解包完成";
                        }
                        else
                        {
                            toolStripStatusLabel1.Text = "解包失败";
                        }
                    }
                    catch (Exception)
                    {
                        toolStripStatusLabel1.Text = "解包失败，文件或文件夹不存在或访问受限";
                    }
                }
            }
        }

        private void rmVoice_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count < 1)
            {
                MessageBox.Show("请选择一条音效再进行删除！");
                return;
            }
            foreach (ListViewItem item in listView1.SelectedItems)
            {
                bind_list.RemoveAt(item.Index);
            }
            RefView();
        }

        private void mdVoicePath_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1 )
            {
                int idx = listView1.SelectedItems[0].Index;
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Mp3|*.mp3|Wave文件(*.wav)|*.wav|WindowsMedia(*.wma)|*.wma|AAC文件(*.aac)|*.aac";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    if (new System.IO.FileInfo(ofd.FileName).Length > 6291456)
                    {
                        MessageBox.Show(this, "哎~文件太大了喂！", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    bind_list[idx].path = ofd.FileName;
                    RefView();
                }
            }
        }
    }
}
