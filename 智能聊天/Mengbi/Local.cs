﻿using System;
using System.Threading;

namespace 新蒙蔽机器人
{
    public class Local : TTS
    {
        private dynamic spVoice;
        delegate void BGJ();
        private BGJ _bgj;
        public Local()
        {
            Type type = Type.GetTypeFromProgID("SAPI.SpVoice");
            spVoice = Activator.CreateInstance(type);
            spVoice.Rate = 0;
        }
        public override void run()
        {
            _bgj = new BGJ(speak);
            _bgj.BeginInvoke(SpeekCallBack,null);
        }

        public override void setVoiceSpeed(double speed)
        {
            voice_speed = speed;
            spVoice.Rate = voice_speed;
        }

        public override void setVoiceVol(double vol)
        {
            voice_vol = vol;
            spVoice.Volume = voice_vol;
        }

        public override void speak()
        {
   
            if (spk_queue.Count > 0)
            {
                try
                {
                    spVoice.Speak(spk_queue.Dequeue());
                }
                catch (Exception)
                {

                }
            }
            else
            {
                Thread.Sleep(500);
            }

        }
 
        private void SpeekCallBack(IAsyncResult ar)
        {
            _bgj.BeginInvoke(SpeekCallBack, null);
        }

        public override void stop()
        {
            base.spk_queue.Clear();
        }
    }
}
