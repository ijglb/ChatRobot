﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 智能聊天
{
    public partial class Frm_Add : Form
    {
        public string Value1 { get; set; }
        public string Value2 { get; set; }

        private int type = 0;

        public Frm_Add()
        {
            InitializeComponent();
        }

        public Frm_Add(int i) : this()
        {
            this.type = i;
        }

        private void Frm_Add_Load(object sender, EventArgs e)
        {
            switch (type)
            {
                case 1:
                    {
                        this.Text += "特定礼物感谢模板";
                        label3.Text = "模板参数（看自己需要使用）：\r\n%gift%代表礼物名\r\n%name%代表送礼物的人\r\n%count%代表送的总数量";
                        break;
                    }
                case 2:
                    {
                        this.Text += "一条自动发言";
                        label1.Visible = false;
                        textBox1.Visible = false;
                        label2.Text = "发言：";
                        label3.Text = "";
                        break;
                    }
                case 3:
                    {
                        this.Text += "一条智能回答";
                        label1.Text = "问题：";
                        label2.Text = "回答：";
                        label3.Text = "同时开启智能回答和图灵的情况下，智能回答的优先级高于图灵！\r\n相似度：接收到的弹幕和设定的问题的相似度大于设定值则会触发回答";
                        break;
                    }
            }
        }

        private void button_OK_Click(object sender, EventArgs e)
        {
            this.Value1 = textBox1.Text.Trim();
            this.Value2 = textBox2.Text.Trim();
            this.DialogResult = DialogResult.OK;
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
